import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';

import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import CV from './Page/Cv';
import Grade from './Page/Grade';
import Login from './Page/Login';
import Nextcv from './Page/Nextcv';
import Header from './component/Header';


const MainRouting = () =>{
    <BrowserRouter>
        <Route path="/processlogin" render={() => {
            localStorage.setItem("islogin", true);
            return <Redirect to="/grade" />
        }} ></Route>
        <Route path="/processlogout" render={() => {
            localStorage.setItem("islogin", false);
            return <Redirect to="/cv" />
        }} ></Route>
        <Route path="/" component={Header} />
        <Route path="/cv" component={CV} />
        <Route path="/nextcv" component={Nextcv} />
        <Route path="/login" component={Login} />
        <PrivateRoute path="/grade" component={Grade} />
        //ภาทที่เราต้องเอาไปใช้ใน component ออกมา 
    </BrowserRouter>
    
}
export default MainRouting;