import React, { useState, useEffect } from "react";
import { enrollSData } from '../Page/gpa'
import Grade from "./Grade";

    import { Select } from 'antd';

    const { Option } = Select;


const Usegpa = () => {
    const [data, setData] = useState([])
    const [currentData, setCurrentData] = useState(-1)


    useEffect(() => {
        setData(enrollSData);

    }, [])

    const onChangeData = (e) => {
        const index = e.target.value
        setCurrentData(index);
    }
// const handleChange=(value) =>{
//       console.log(`selected ${value}`);
//     }
    


    return (


        <div className="container">
            <nav class="navbar navbar-dark bg-dark" >
                <h4 id="headtable"> Select to </h4>


                <select class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onChange={onChangeData}>
                    <option value={-1}>All</option>
                    {
                        data.map((dataItem, index) => {
                            return (
                                <option value={index}>{dataItem.noSemester}</option>
                            )
                        })}
                </select>

                       {/* <div>
                    <Select defaultValue="lucy" style={{ width: 120 }} onChange={handleChange}>
                    {
                        data.map((dataItem, index) => {
                            return (
                        <Option value={index}>{dataItem.noSemester}</Option>
                        )
                    })}
    
                    </Select>
                </div>  */}

            </nav>
            {
                currentData == -1 ?
                    <Grade sentGrade={data} />
                    :
                    <Grade sentGrade={[data[currentData]]} />


            }




        </div>
    )




}
export default Usegpa;
