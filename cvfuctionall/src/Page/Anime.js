import React, { useState, useEffect } from "react";
import Searchani from './Searchani'
import { Input } from 'antd';
import { Menu, Dropdown, Button, Icon, message } from 'antd';
const { Search } = Input;


const Anime = () => {
  const [dataAnni, setDataAnni] = useState([])
  const [searchData, setsearchData] = useState('naruto')
  const [isLoding, setislodeding] = useState(false);
  const[saveType,setSaveType]=useState('Type')

  useEffect(() => {
    fetchData();
  }, [searchData])

const fetchData = () => {
    let urllist = '';
    if (searchData) {
      urllist = 'https://api.jikan.moe/v3/search/anime?q=' + searchData;
    } else {
      urllist = 'https://api.jikan.moe/v3/search/anime?q=naruto';
    }
    fetch(urllist)
      .then(response => response.json())
      .then(data => {
        setTimeout(() => {
          setislodeding(false);
        }, 2000);
        setDataAnni(data.results);
      })
      .catch(error => console.log(error));
  }

  const onSearchani = (e) => {
    setsearchData(e);
    setislodeding(true);
    fetchData();
  }

  const handleMenuClick = (e) => {
   
    setSaveType(e.key)
  }

  const menu = (dataAnni) => {
    let typeList = [];
    dataAnni.map(ani => {

      if (!typeList.includes(ani.type))
        typeList.push(ani.type)
        
    });
    
    return (
      <Menu onClick={handleMenuClick}>
        {
          typeList.map((type, index) => {
            return (
              <Menu.Item value={index} key={type}>
                <Icon  type="user" />
                {type}
              </Menu.Item>
            ) 
          })}
      </Menu>
    )
  }
console.log(dataAnni)
  return (

    <div class="container">
      
      <div>

        <Search
          placeholder="input search text"
          onSearch={onSearchani}
          style={{ width: 200 }}
          loading={isLoding}
        />
        <div id='buttonType'>
        <div id="components-dropdown-demo-dropdown-button">
          <Dropdown overlay={menu(dataAnni)}>
          <Button>
  {saveType} <Icon type="down" />
      </Button>

          </Dropdown>
        </div>
        </div>
      </div>
      <div>
        <Searchani sentDataani={dataAnni}   sentDataSearch={searchData} lode={isLoding} sentDataType={saveType}/>
      </div>

    </div>

  )
}




export default Anime;