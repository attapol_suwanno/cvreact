import React from "react";


const Grade = (props) => {

    return (
        <div>
                 <div class="row" >
            <div class="col-3">
                <img class="shadow p-3 mb-5 bg-white rounded" id="pic1" src={require('../assets/83750688_464443824230088_8036642130241781760_n.jpg')}/>
            </div>
            <div class="col-9">
                <div class=" row " >
                    <div class="col">
                        <h1>Grade Report</h1>
                    </div>
                </div>
                <div class=" row " >
                    <div class="col">
                        <p> ID:602100181 <br/>
                            Name : Attapol Suwanno</p>
                    </div>
                </div>
                <div class=" row ">
                    <div class="col">
                        <p> Degree: bachelor of science </p>
                    </div>

                </div>
                <div class=" row " >
                    <div class="col">
                        <p>Faculty: college of are media and Technology<br/>
                            Major: Modern Management Media and Technology </p>
                    </div>
                </div>
            </div>
        </div>
            
            {props.sentGrade.map((semester, index) => {
                console.log(semester);
                const { noSemester,subjects, gpas } = semester;
                return (
                    <div class="row justify-content-center " >
                        <div class="col-7 ">
                            <h2>{noSemester} </h2>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Course no</th>
                                        <th scope="col">Course Title</th>
                                        <th scope="col">Credit</th>
                                        <th scope="col">Grade</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {subjects.map((data, index) => {
                                        return (
                                            <tr key={index}>
                                                <th scope="row">{index+1}</th>
                                                <td>{data.subjectId}</td>
                                                <td>{data.subjectName}</td>
                                                <td>{data.credit}</td>
                                                <td>{data.grade}</td>
                                            </tr>
                                        )
                                    })
                                    }

                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-center " >
                            <div class="col-6 ">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                            <th scope="col">ALL GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr key={index}>
                                            <th scope="row">Semester 1/2560</th>
                                            <td>{gpas.currensem.CA}</td>
                                            <td>{gpas.currensem.CE}</td>
                                            <td>{gpas.cumulative.CA}</td>
                                            <td>{gpas.cumulative.CE}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>






                )

            }

            )
            }
        </div>

    )
}




export default Grade;