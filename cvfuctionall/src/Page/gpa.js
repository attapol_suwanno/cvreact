
export const enrollSData = [
    {
        noSemester:'Semester 1/2560',
        subjects: [
            {
                subjectId: '001101',
                subjectName: 'FUNDAMENTAL ENGLISH 1',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '109100',
                subjectName: 'MAN AND ART',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '851100',
                subjectName: 'INTRO TO COMMUNICATION',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '109100',
                subjectName: 'INFO SYSTEM FOR ORG MGMT',
                credit: 3.00,
                grade: 'A'
            },
            {
                subjectId: '951100',
                subjectName: 'MODERN LIFE AND ANIMATION',
                credit: 3.00,
                grade: 'A'
            },
            {
                subjectId: '954140',
                subjectName: 'IT LITERACY',
                credit: 3.00,
                grade: 'A'
            }
        ],
        gpas: {
            currensem: {
                CA: 18.00,
                CE: 18.00
            },
            cumulative: {
                CA: 36.00,
                CE: 36.00

            }
        },
    }, 
    {noSemester:'Semester 2/2560',
        subjects: [
            {
                subjectId: '001101',
                subjectName: 'FUNDAMENTAL ENGLISH 1',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '109100',
                subjectName: 'MAN AND ART',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '851100',
                subjectName: 'INTRO TO COMMUNICATION',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '109100',
                subjectName: 'INFO SYSTEM FOR ORG MGMT',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '951100',
                subjectName: 'MODERN LIFE AND ANIMATION',
                credit: '3.00',
                grade: 'A'
            },
            {
                subjectId: '954140',
                subjectName: 'IT LITERACY',
                credit: '3.00',
                grade: 'A'
            }
        ], gpas: {
            currensem: {
                CA: 14.00,
                CE: 19.00
            },
            cumulative: {
                CA: 30.00,
                CE: 33.00

            }
        }



    }
]



