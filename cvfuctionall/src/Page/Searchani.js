import React, { useState, useEffect } from "react";
import { List, Avatar, Icon } from 'antd';
import { Spin } from 'antd';


const listData = [];
for (let i = 0; i < 23; i++) {
    listData.push({
        href: 'http://ant.design',
        title: `ant design part ${i}`,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        description:
            'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content:
            'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    });
}

const IconText = ({ type, text }) => (
    <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
    </span>
);


const Searchani = (props) => {
    return (
        <div class="container">
            <div>
                <List
                    itemLayout="vertical"
                    size="large"
                    loading={props.lode}
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 3,
                    }}
                    dataSource={
                        props.sentDataType == 'Type' ?
                            props.sentDataani
                            :
                            props.sentDataani.filter((data,index)=>{
                                return (data.type ==props.sentDataType)
                            })
                            // props.sentDataani.filter(m => m.title.match(props.sentDataSearch))
                        // props.sentDataani.type((data,index)=>{
                        //     const {typeData}=data;
                        //     console.log(typeData.types)
                        //     return(typeData.type == props.sentDataType.key)
                        // })
                    }
                    renderItem={item => (
                        <List.Item

                            key={item.title}

                            extra={
                                <img
                                    width={272}
                                    alt="logo"
                                    src={item.image_url}
                                />
                            }
                        >
                            <List.Item.Meta

                                title={<a href={item.href}>{item.title}</a>}
                                description={item.synopsis}
                                Tv={item.score}
                            />
                            {item.content}
                            <div>
                                <p>Score:{item.score}</p>
                                <p>StartDate:{item.start_date}</p>
                                <p>EndDate:{item.end_date}</p>
                                <p>Members:{item.members}</p>
                                <p>Rated:{item.rated}</p>
                                <p>Rated:{item.type}</p>

                            </div>
                        </List.Item>
                    )}
                />,
            </div>


        </div>

    )
}


export default Searchani;