import React, { Component } from 'react';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter, Route, Redirect } from 'react-router-dom'

localStorage.setItem('islogin', false)
const PrivateRoute = ({ path, component: Component }) => (
    <Route path={path} render={(props) => {
        const islogin = localStorage.getItem('islogin')

        if (islogin == 'true')
            return <Component {...props} />

        else
            return <Redirect to="/login" />


    }} />
)
export default PrivateRoute ;