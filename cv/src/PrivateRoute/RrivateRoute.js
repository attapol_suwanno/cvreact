import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';

import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import CV from './Page/Cv';
import Grade from './Page/Grade';
import Login from './Page/Login';
import Nextcv from './Page/Nextcv';
import Header from './component/Header';


const PrivateRoute = ({ path, component: Component }) => (
    <Route path={path} render={(props) => {
        const islogin = localStorage.getItem('islogin')

        if (islogin == 'true')
            return <Component {...props} />

        else
            return <Redirect to="/login" />


    }} />
)
export default PrivateRoute ;