import React from "react";




class Grade extends React.Component {

    render() {

        return (
            <div>
                <body>
                    <div class="container">
                        <div class="row justify-content-end" >
                            <div class="col-1">
                                <input type="button" class="btn btn-outline-dark" value="Logout" onClick={() => {
                                    window.location = "/processlogout"}
                                } />
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-3">
                                <img class="shadow p-3 mb-5 bg-white rounded" id="pic1" src="" />
                            </div>

                            <div class="col-9">
                                <div class=" row " >
                                    <div class="col">
                                        <h1>Grade Report</h1>

                                    </div>

                                </div>
                                <div class=" row " >
                                    <div class="col">
                                        <p> ID:602100181
                            Name : Attapol Suwanno</p>
                                    </div>

                                </div>
                                <div class=" row " >
                                    <div class="col">
                                        <p> Degree: bachelor of science </p>
                                    </div>

                                </div>
                                <div class=" row " >
                                    <div class="col">
                                        <p>Faculty: college of are media and Technology
                            Major: Modern Management Media and Technology </p>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <h3>Semester 1/2560</h3>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Course no</th>
                                            <th scope="col">Course Title</th>
                                            <th scope="col">Credit</th>
                                            <th scope="col">Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>001101</td>
                                            <td>FUNDAMENTAL ENGLISH 1</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>109100</td>
                                            <td>MAN AND ART </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>851100</td>
                                            <td>INTRO TO COMMUNICATION </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>951100</td>
                                            <td>MODERN LIFE AND ANIMATION</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>954100</td>
                                            <td>INFO SYSTEM FOR ORG MGMT</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">6</th>
                                            <td>954140</td>
                                            <td>IT LITERACY</td>
                                            <td>3.00</td>
                                            <td>A</td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                            <th scope="col">ALL GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Semester 1/2560</th>
                                            <td>18.00</td>
                                            <td>18.00</td>
                                            <td>3.50</td>
                                            <td>3.50</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-start " >
                            <div class=" col-6 ">
                                <h3>Semester 2/2560</h3>
                            </div>
                        </div>
                        <div class="row justify-content-start " >
                            <div class=" col-6 ">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Course no</th>
                                            <th scope="col">Course Title</th>
                                            <th scope="col">Credit</th>
                                            <th scope="col">Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>001102</td>
                                            <td>FUNDAMENTAL ENGLISH 2</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>050104</td>
                                            <td>MAN AND THE MODERN WORLD</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>201111</td>
                                            <td>THE WORLD OF SCIENCE</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>206171</td>
                                            <td>GENERAL MATHEMATICS 1</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>954141</td>
                                            <td>INFORM AND COMM TECH</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">6</th>
                                            <td>954142</td>
                                            <td>FUNDA COM PROGRAM FOR MM</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">7</th>
                                            <td>955100</td>
                                            <td>LEARNING THROUGH ACTIVITIES 1</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-start " >
                            <div class=" col-6 ">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                            <th scope="col">ALL GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Semester 2/2560</th>
                                            <td>19.00</td>
                                            <td>19.00</td>
                                            <td>3.55</td>
                                            <td>3.50</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <h3>Semester 1/2561</h3>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Course no</th>
                                            <th scope="col">Course Title</th>
                                            <th scope="col">Credit</th>
                                            <th scope="col">Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>001201</td>
                                            <td>CRIT READ AND EFFEC WRITE </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>208253</td>
                                            <td>ELEMENTARY STATISTICS </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>954245</td>
                                            <td>DATA MANAGEMENT </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>954246</td>
                                            <td>ADV COMP PROGRAMMING FOR MM </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>954260</td>
                                            <td>KNOWLEDGE MANAGEMENT SYSTEM </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">6</th>
                                            <td>954270</td>
                                            <td>ELEMENTARY BPM </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                            <th scope="col">ALL GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Semester 1/2561</th>
                                            <td>18.00</td>
                                            <td>18.00</td>
                                            <td>3.75</td>
                                            <td>3.50</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-start ">
                            <div class="col-6 ">
                                <h3>Semester 2/2561</h3>
                            </div>
                        </div>
                        <div class="row justify-content-start ">
                            <div class="col-6 ">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Course no</th>
                                            <th scope="col">Course Title</th>
                                            <th scope="col">Credit</th>
                                            <th scope="col">Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>001201</td>
                                            <td>CRIT READ AND EFFEC WRITE </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>001229</td>
                                            <td>ENGLISH FOR MEDIA ARTS </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>208263</td>
                                            <td>ELEMENTARY STATISTICS </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>954240</td>
                                            <td>WEB PROGRAMMING </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>954244</td>
                                            <td>STRUCTURAL ANALYSIS AND DESIGN </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">6</th>
                                            <td>954310</td>
                                            <td>INFO SYS FOR ERP</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">7</th>
                                            <td>954340</td>
                                            <td>ENTERPRISE DATABASE SYSTEM</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-start " >
                            <div class="col-6 ">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                            <th scope="col">ALL GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Semester 2/2561</th>
                                            <td>21.00</td>
                                            <td>21.00</td>
                                            <td>3.85</td>
                                            <td>3.50</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <h3>Semester 1/2562</h3>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Course no</th>
                                            <th scope="col">Course Title</th>
                                            <th scope="col">Credit</th>
                                            <th scope="col">Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>057127</td>
                                            <td>BADMIN FOR LIFE & EXERC </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>206171</td>
                                            <td>GENERAL MATHEMATICS 1 </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>954326</td>
                                            <td>IT IN EVENT MGMT </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>A</td>
                                            <td>ANALYSIS & DESIGN MATS MGMT </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>954374</td>
                                            <td>SD FOR DIGITAL MARKET </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">6</th>
                                            <td>954477</td>
                                            <td>INFO SYS FOR SCM AND CRM </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">7</th>
                                            <td>954426</td>
                                            <td>INTRODUCTION TO E-SERVICE </td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">8</th>
                                            <td>954477</td>
                                            <td>IT FOR PRODUCTION SYS</td>
                                            <td>3.00</td>
                                            <td>A</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row justify-content-end " >
                            <div class="col-6 ">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                            <th scope="col">ALL GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Semester 1/2562</th>
                                            <td>22.00</td>
                                            <td>22.00</td>
                                            <td>3.80</td>
                                            <td>4.00</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" colspan="2">Cumulative</th>
                                            <td>89.00</td>
                                            <td colspan="1">3.00</td>
                                            <td>4.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-12">
                                <footer class="page-footer font-small blue">
                                    <div class="footer-copyright text-center py-3">© 2020 Copyright:
                        <a> Attapol Suwanno</a>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </body>


            </div>
        )

    }
}

export default Grade;