import React from "react";
import "../App.css";
// import ImgBack from "../../assets/78713698_2660079620752131_5117077324113641472_o.jpg"



class CV extends React.Component {

    render() {
        return (
            <div>
                <body>
                    <div class="container">

                        <div id="table1" class="row" >
                            <div class="col-3">
                                <a href="https://www.camt.cmu.ac.th/th/" target="_blank"></a>
                                <img class="shadow p-3 mb-5 bg-white rounded"
                                    src=" https://www.camt.cmu.ac.th/th/logo/camt_digital.jpg" />
                            </div>
                        </div>


                        <div id="table2" class="row justify-content-start" >
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        {/* <img src={ImgBack}/> */}
                                    </div>
                                    {/* <div class="carousel-item">
                                        <img src="../../assets/181/IMG_06741111111111.jpg" class="d-block w-100" />
                                    </div>
                                    <div class="carousel-item">
                                        <img src={require("../../assets/nkmlmlml.jpg")} class="d-block  w-100" />
                                    </div> */}
                                </div>
                                <a class="carousel-control-prev" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                <div class="card-img-overlay">
                                    <h1 class="card-title">ATTAPOL SUWANNO</h1>
                                </div>
                            </div>
                        </div>

                        <div id="table3" class="row justify-content-center" >
                            <div class="col-6">
                                <img class="shadow p-3 mb-5 bg-white rounded" id="img1"
                                    src="../181/75262123_2624077024352391_5463699717684723712_o.jpg" />
                            </div>
                            <div class="offset-md-1 col-6">
                                <div class=" row " >
                                    <div class="col-12">
                                        <h1>BIOGRAPHY</h1>
                                    </div>
                                </div>
                                <div class="shadow-lg p-3 mb-5 bg-white rounded" class=" row " >
                                    <div class="col-12">
                                        <h2>Information</h2>
                                        <p> Name : Attapol Suwanno </p>
                                        <p> Nickname : min </p>
                                        <p>Date of birdth: 02/01/1998</p>
                                        <p>Weight/Higth : 56 kg./174 cm.</p>
                                    </div>
                                </div>
                                <div class="shadow p-3 mb-5 bg-white rounded" class=" row "
                                >
                                    <div class="col-12">
                                        <h2>Education</h2>
                                        <p>2005-2009 : Hnongthakwoy School </p>
                                        <p>2010-2019 : Phongpattanawittayakhow School </p>
                                        <p>2017-now : Chiangmai University </p>
                                    </div>
                                </div>
                                <div class="shadow-lg p-3 mb-5 bg-white rounded" class=" row ">
                                    <div class="col-12">
                                        <h2>Contect</h2>
                                        <p>Telephone NB : 0969300228 </p>
                                        <p>Email : Attapol_suwanno@cmu.ac.th </p>
                                        <p>FB : Min Ton </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </body >

            </div >
        )

    }
}

export default CV;