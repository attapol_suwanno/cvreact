import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';

import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import CV from './Page/Cv';
import Grade from './Page/Grade';
import Login from './Page/Login';
import Nextcv from './Page/Nextcv';
import Header from './component/Header';


localStorage.setItem('islogin', false)

// howt o use localstorege

// setter
// localStorage.setItem('a','hello,world')
// เก็บสถานะ ในการ login

const PrivateRoute = ({ path, component: Component }) => (
    <Route path={path} render={(props) => {
        const islogin = localStorage.getItem('islogin')

        if (islogin == 'true')
            return <Component {...props} />

        else
            return <Redirect to="/login" />


    }} />
)

const MainRouting =
    <BrowserRouter>
        <Route path="/processlogin" render={() => {
            localStorage.setItem("islogin", true);
            return <Redirect to="/grade" />
        }} ></Route>
        <Route path="/processlogout" render={() => {
            localStorage.setItem("islogin", false);
            return <Redirect to="/cv" />
        }} ></Route>
        <Route path="/" component={Header} />
        <Route path="/cv" component={CV} />
        <Route path="/nextcv" component={Nextcv} />
        <Route path="/login" component={Login} />
        <PrivateRoute path="/grade" component={Grade} />
    </BrowserRouter>




//index.js มีแค่ด้านล่างนี้ อันข้างบนสามารถ เอามาแยกไฟลในรุปแบบ component


ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();