import React from "react";
import "../App.css";
import reqwest from '../App';
import { Input, List, Avatar, Skeleton } from 'antd';
import { Layout, Breadcrumb ,BackTop} from 'antd';
import Headerpage from '../Component/Header';
import Footerpage from '../Component/Footer'

const { Content } = Layout;
const { Search } = Input;
const count = 3;
const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat&noinfo`;

class Userpage extends React.Component {

  constructor() {
    super();
    this.state = {
      users: [],
      searchdata: ""

    };
  }

  fechUserData = () => {

    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(data => {

        this.setState({ users: data });
        
      })
  }

  onSearchname = (event) => {
    const getDatasearch = event.target.value;
    this.setState({ searchdata: getDatasearch })
    this.fechUserData();
  }

  componentDidMount() {
    this.fechUserData();
    this.getData(res => {
      this.setState({
        data: res.results
      });
    });
  }

  getData = callback => {
    reqwest({
      url: fakeDataUrl,
      type: 'json',
      method: 'get',
      contentType: 'application/json',
      success: res => {
        callback(res);
      },
    });
  };

  render() {
    const { users, searchdata } = this.state;

    return (
      <div>
        <Layout>
          <div>
            <Headerpage />
          </div>
          <Content style={{ padding: '0 50px', marginTop: 64 }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
              <Search
                placeholder="input search text"
                onSearch={value => this.setState({ ...this.setState, searchdata: value })}
                onChange={this.onSearchname}
                style={{ width: 200 }}
              />
              <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                dataSource={searchdata == '' ?
                  this.state.users
                  :
                  this.state.users.filter(m => m.name.match(this.state.searchdata))
                }
                renderItem={item => (
                  <List.Item
                    actions={[<a href={'/users/' + item.id + '/todo'} >Todo</a>, <a href={'/album/' + item.id} >Album</a>]}
                  >
                    <Skeleton avatar title={false} loading={item.loading} active>
                      <List.Item.Meta
                        avatar={
                          <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                        }
                        title={<a href={'/users/' + item.id + '/todo'}>{item.name}</a>}
                        description={item.email}
                      />
                    </Skeleton>
                  </List.Item>
                )}
              />
            </div>
          </Content>
          <div>
            <Footerpage />
          </div>
          <div>
            <BackTop />
            Scroll down to see the bottom-right
            <strong style={{ color: 'rgba(64, 64, 64, 0.6)' }}> gray </strong>
            button.
          </div>

        </Layout>
      </div>
    );
  }
}





export default Userpage;