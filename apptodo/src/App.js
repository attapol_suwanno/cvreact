import React from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Userpage from './Page/Userpage'
import TodoPage from './Page/Todopage'
import Albumspage from './Page/Albumspage'
import AlbumPhotopage from './Page/AlbumPhotopage'

function App() {
  return (
    <div>
      <BrowserRouter>
        <Route path="/" component={Userpage} exact={true} />
        <Route path="/users/:user_id/todo" component={TodoPage} />
        {/* <Route path="/album/:user_id" component={Albumspage} exact={true} />
        <Route path="/album/:album_id/photo" component={AlbumPhotopage} /> */}

      </BrowserRouter>

    </div>
  )
}

export default App;
